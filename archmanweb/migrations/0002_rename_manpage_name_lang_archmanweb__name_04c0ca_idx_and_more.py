# Generated by Django 5.0.9 on 2024-10-13 14:48

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("archmanweb", "0001_initial"),
    ]

    operations = [
        migrations.RenameIndex(
            model_name="manpage",
            new_name="archmanweb__name_04c0ca_idx",
            old_fields=("name", "lang"),
        ),
        migrations.RenameIndex(
            model_name="manpage",
            new_name="archmanweb__section_0e8e6b_idx",
            old_fields=("section", "name"),
        ),
        migrations.RenameIndex(
            model_name="manpage",
            new_name="archmanweb__lang_0c4ca0_idx",
            old_fields=("lang", "name", "section"),
        ),
        migrations.RenameIndex(
            model_name="manpage",
            new_name="archmanweb__section_c6738f_idx",
            old_fields=("section", "name", "lang"),
        ),
        migrations.RenameIndex(
            model_name="manpage",
            new_name="archmanweb__name_40ba48_idx",
            old_fields=("name", "lang", "section"),
        ),
        migrations.RenameIndex(
            model_name="symboliclink",
            new_name="archmanweb__from_se_0d698b_idx",
            old_fields=("from_section", "from_name"),
        ),
        migrations.RenameIndex(
            model_name="symboliclink",
            new_name="archmanweb__from_se_526737_idx",
            old_fields=("from_section", "from_name", "lang"),
        ),
        migrations.RenameIndex(
            model_name="symboliclink",
            new_name="archmanweb__from_na_214e63_idx",
            old_fields=("from_name", "lang"),
        ),
    ]
